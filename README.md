# Overview

A skeleton [APT/PPA][0] repository for various software needs in [Kelly Bolton's Labratory][1].  The debian packages in this repository are based on the [bolton-lab-debian-recipes][5] repository. The packages are targeted towards [Debian GNU/Linux 11 (bullseye)][2].

# Usage

## Adding a new debian package to the repository

Copy the debian package you want to add to the repo into the top-level directory of this repository and then run these docker commands:

    docker build -t apt:v1 .
    docker run -i -t -v $PWD:/build --rm apt:v1 <package.deb>

Aftewards, do the following:

1.  delete the debian package you copied into the top-level directory of this repository
2.  Add and commit the remaining contents of this directory into the git repository.


## Using the apt repository and installing a package

Run the following commands, or add them into your [Dockerfile][3] like so:

```bash
# assuming you're running as the root user, otherwise prefix the commands with "sudo"

# ensure the system is up to date
apt-get update -qq

# ensure APT has the ability to access a repository via https
apt-get -y install apt-transport-https ca-certificates

# add the apt repository library to the main operating systems apt list
echo "deb [trusted=yes] https://gitlab.com/indraniel/debian-apt-repo-1/raw/main bullseye main" | tee -a /etc/apt/sources.list

# get the latest updates from all the apt repositories
apt-get update -qq

# see all the available packages to install
apt-cache search bolton-lab

# install an example package
apt-get -y install bolton-lab-python-3.11.1
```

# References

For more details on how this repository was setup, see also:

* [HOWTO: Create debian repositories with reprepro][4]

[0]: https://wiki.debian.org/DebianRepository/SetupWithReprepro
[1]: https://oncology.wustl.edu/people/faculty/Bolton/Bolton_Bio.html
[2]: https://www.debian.org
[3]: https://docs.docker.com/engine/reference/builder/
[4]: https://blog.packagecloud.io/how-to-create-debian-repository-with-reprepro/
[5]: https://gitlab.com/indraniel/bolton-lab-debian-recipes
