FROM debian:bullseye-slim
MAINTAINER Indraniel Das <idas@wustl.edu>

# Volumes
VOLUME /build

# dpkg-dev
# Install basic build dependencies
RUN apt-get update -qq && apt-get -y install \
    git \
    reprepro \
    --no-install-recommends

WORKDIR /build

ENTRYPOINT [ "reprepro", "-b", ".", "includedeb", "bullseye" ]
CMD [ "" ]

